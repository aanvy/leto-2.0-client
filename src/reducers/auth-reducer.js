import {
  GET_USERS_FAILED,
  GET_USERS_INIT,
  GET_USERS_SUCCESS,
  SET_AUTH
} from '../actions/Actions';

export const initialState = {
  users: [],
  isAuthenticated: false
};
const handlers = {
  [GET_USERS_INIT]: state => ({
    ...state,
    users: []
  }),
  [GET_USERS_SUCCESS]: (state, action) => ({
    ...state,
    users: action.response
  }),
  [GET_USERS_FAILED]: state => ({
    ...state,
    users: []
  }),
  [SET_AUTH]: state => ({
    ...state,
    isAuthenticated: true
  })
};
export default function authReducer(state = initialState, action) {
  const handler = handlers[action.type];
  if (!handler) return state;
  return { ...state, ...handler(state, action) };
}
