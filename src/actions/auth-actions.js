import getUsersAPI from '../api/auth-api';

export const GET_USERS_INIT = 'GET_USERS_INIT';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const GET_USERS_FAILED = 'GET_USERS_FAILED';
export const SET_AUTH = 'SET_AUTH';

export function getUsers() {
  return dispatch => {
    dispatch({ type: GET_USERS_INIT });
    getUsersAPI()
      .then(response => dispatch({ type: GET_USERS_SUCCESS, response }))
      .catch(error => dispatch({ type: GET_USERS_FAILED, error }));
  };
}

export function setAuth() {
  return { type: SET_AUTH };
}
