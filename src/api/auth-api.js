export default function getUsersAPI(body) {
  return fetch(`https://jsonplaceholder.typicode.com/users`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify(body)
  })
    .then(res => res.json())
    .catch(err => err);
}
