/* eslint-disable */
import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import classNames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Hidden from '@material-ui/core/Hidden';
import Collapse from '@material-ui/core/Collapse';
import Icon from '@material-ui/core/Icon';
import HeaderLinks from '../header/HeaderLinks';
import SidebarWrapper from './SidebarWrapper';
import {
  drawerWidth,
  drawerMiniWidth,
  transition,
  boxShadow,
  defaultFont,
  primaryColor,
  primaryBoxShadow,
  infoColor,
  successColor,
  warningColor,
  dangerColor,
  roseColor,
  cornsilkColor,
  darkgreyColor,
  liteseagreenColor,
  salmonColor
} from '../../assets/theme/leto';
import { red } from '@material-ui/core/colors';

const sidebarStyle = theme => ({
  drawerPaperRTL: {
    [theme.breakpoints.up('md')]: {
      left: 'auto !important',
      right: '0 !important'
    },
    [theme.breakpoints.down('sm')]: {
      left: '0  !important',
      right: 'auto !important'
    }
  },
  drawerPaper: {
    border: 'none',
    position: 'fixed',
    top: '0',
    bottom: '0',
    left: '0',
    zIndex: '1032',
    transitionProperty: 'top, bottom, width',
    transitionDuration: '.2s, .2s, .35s',
    transitionTimingFunction: 'linear, linear, ease',
    // overflow: 'auto',
    ...boxShadow,
    width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: drawerWidth,
      position: 'fixed',
      height: '100%'
    },
    [theme.breakpoints.down('sm')]: {
      width: drawerWidth,
      ...boxShadow,
      position: 'fixed',
      display: 'block',
      top: '0',
      height: '100vh',
      right: '0',
      left: 'auto',
      zIndex: '1032',
      visibility: 'visible',
      overflowY: 'visible',
      borderTop: 'none',
      textAlign: 'left',
      paddingRight: '0px',
      paddingLeft: '0',
      transform: `translate3d(${drawerWidth}px, 0, 0)`,
      ...transition
    },
    '&:before,&:after': {
      position: 'absolute',
      zIndex: '3',
      width: '100%',
      height: '100%',
      content: '""',
      display: 'block',
      top: '0'
    }
  },
  blackBackground: {
    color: '#FFFFFF',
    '&:after': {
      background: '#0747A6'
    }
  },
  blueBackground: {
    color: '#FFFFFF',
    '&:after': {
      background: '#00acc1',
      opacity: '.93'
    }
  },
  whiteBackground: {
    color: '#3C4858',
    '&:after': {
      background: '#FFFFFF',
      opacity: '.93'
    }
  },
  darkgreyBackground: {
    color: '#FFFFFF',
    '&:after': {
      background: darkgreyColor,
      opacity: '.93'
    }
  },
  liteBackground: {
    color: darkgreyColor,
    '&:after': {
      background: cornsilkColor,
      opacity: '.93'
    }
  },

  whiteAfter: {
    '&:after': {
      backgroundColor: 'hsla(0,0%,71%,.3) !important'
    }
  },
  drawerPaperMini: {
    width: `${drawerMiniWidth}px!important`
  },
  logo: {
    padding: '15px 0px',
    margin: '0',
    display: 'block',
    position: 'relative',
    zIndex: '4',
    '&:after': {
      content: '""',
      position: 'absolute',
      bottom: '0',
      height: '1px',
      right: '15px',
      width: 'calc(100% - 30px)',
      backgroundColor: 'hsla(0,0%,100%,.3)'
    }
  },
  logoMini: {
    transition: 'all 300ms linear',
    opacity: 1,
    float: 'left',
    textAlign: 'center',
    width: '30px',
    display: 'inline-block',
    maxHeight: '30px',
    marginLeft: '22px',
    marginRight: '18px',
    marginTop: '7px',
    color: 'inherit'
  },
  logoMiniRTL: {
    float: 'right',
    marginRight: '30px',
    marginLeft: '26px'
  },
  logoNormal: {
    ...defaultFont,
    transition: 'all 300ms linear',
    display: 'block',
    opacity: '1',
    transform: 'translate3d(0px, 0, 0)',
    textDecoration: 'none',
    padding: '5px 0px',
    fontSize: '18px',
    whiteSpace: 'nowrap',
    fontWeight: '400',
    lineHeight: '30px',
    overflow: 'hidden',
    '&,&:hover,&:focus': {
      color: 'inherit'
    }
  },
  logoNormalRTL: {
    textAlign: 'right'
  },
  logoNormalSidebarMini: {
    opacity: '0',
    transform: 'translate3d(-25px, 0, 0)'
  },
  logoNormalSidebarMiniRTL: {
    transform: 'translate3d(25px, 0, 0)'
  },
  img: {
    width: '35px',
    verticalAlign: 'middle',
    border: '0'
  },
  background: {
    position: 'absolute',
    zIndex: '1',
    height: '100%',
    width: '100%',
    display: 'block',
    top: '0',
    left: '0',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    transition: 'all 300ms linear'
  },
  list: {
    marginTop: '15px',
    paddingLeft: '0',
    paddingTop: '0',
    paddingBottom: '0',
    marginBottom: '0',
    listStyle: 'none',
    color: 'inherit',
    '&:before,&:after': {
      display: 'table',
      content: '" "'
    },
    '&:after': {
      clear: 'both'
    }
  },
  item: {
    color: 'inherit',
    position: 'relative',
    display: 'block',
    textDecoration: 'none',
    margin: '0',
    padding: '0'
  },
  userItem: {
    '&:last-child': {
      paddingBottom: '0px'
    }
  },
  itemLink: {
    paddingLeft: '10px',
    paddingRight: '10px',
    transition: 'all 300ms linear',
    margin: '10px 15px 0',
    borderRadius: '3px',
    position: 'relative',
    display: 'block',
    padding: '10px 15px',
    backgroundColor: 'transparent',
    textDecoration: 'none',
    ...defaultFont,
    width: 'auto',
    '&:hover': {
      outline: 'none',
      backgroundColor: 'rgba(200, 200, 200, 0.2)',
      boxShadow: 'none'
    },
    '&,&:hover,&:focus': {
      color: 'inherit'
    }
  },
  itemIcon: {
    color: 'inherit',
    width: '30px',
    height: '24px',
    float: 'left',
    position: 'inherit',
    top: '3px',
    marginRight: '15px',
    textAlign: 'center',
    verticalAlign: 'middle',
    opacity: '0.8'
  },
  itemIconRTL: {
    float: 'right',
    marginLeft: '15px',
    marginRight: '1px'
  },
  itemText: {
    color: 'inherit',
    ...defaultFont,
    margin: '0',
    lineHeight: '30px',
    fontSize: '14px',
    transform: 'translate3d(0px, 0, 0)',
    opacity: '1',
    transition: 'transform 300ms ease 0s, opacity 300ms ease 0s',
    position: 'relative',
    display: 'block',
    height: 'auto',
    whiteSpace: 'nowrap'
  },
  userItemText: {
    lineHeight: '22px'
  },
  itemTextRTL: {
    marginRight: '45px',
    textAlign: 'right'
  },
  itemTextMini: {
    transform: 'translate3d(-25px, 0, 0)',
    opacity: '0'
  },
  itemTextMiniRTL: {
    transform: 'translate3d(25px, 0, 0) !important'
  },
  collapseList: {
    marginTop: '0'
  },
  collapseItem: {
    position: 'relative',
    display: 'block',
    textDecoration: 'none',
    margin: '10px 0 0 0',
    padding: '0'
  },
  collapseActive: {
    outline: 'none',
    backgroundColor: 'rgba(200, 200, 200, 0.2)',
    boxShadow: 'none'
  },
  collapseItemLink: {
    transition: 'all 300ms linear',
    margin: '0 15px',
    borderRadius: '3px',
    position: 'relative',
    display: 'block',
    padding: '10px',
    backgroundColor: 'transparent',
    textDecoration: 'none',
    ...defaultFont,
    width: 'auto',
    '&:hover': {
      outline: 'none',
      backgroundColor: 'rgba(200, 200, 200, 0.2)',
      boxShadow: 'none'
    },
    '&,&:hover,&:focus': {
      color: 'inherit'
    }
  },
  collapseItemMini: {
    color: 'inherit',
    ...defaultFont,
    textTransform: 'uppercase',
    width: '30px',
    marginRight: '15px',
    textAlign: 'center',
    letterSpacing: '1px',
    position: 'relative',
    float: 'left',
    display: 'inherit',
    transition: 'transform 300ms ease 0s, opacity 300ms ease 0s',
    fontSize: '14px'
  },
  collapseItemMiniRTL: {
    float: 'right',
    marginLeft: '30px',
    marginRight: '1px'
  },
  collapseItemText: {
    color: 'inherit',
    ...defaultFont,
    margin: '0',
    position: 'relative',
    transform: 'translateX(0px)',
    opacity: '1',
    whiteSpace: 'nowrap',
    display: 'block',
    transition: 'transform 300ms ease 0s, opacity 300ms ease 0s',
    fontSize: '14px'
  },
  collapseItemTextRTL: {
    textAlign: 'right'
  },
  collapseItemTextMiniRTL: {
    transform: 'translate3d(25px, 0, 0) !important'
  },
  collapseItemTextMini: {
    transform: 'translate3d(-25px, 0, 0)',
    opacity: '0'
  },
  caret: {
    marginTop: '13px',
    position: 'absolute',
    right: '18px',
    transition: 'all 150ms ease-in',
    display: 'inline-block',
    width: '0',
    height: '0',
    marginLeft: '2px',
    verticalAlign: 'middle',
    borderTop: '4px solid',
    borderRight: '4px solid transparent',
    borderLeft: '4px solid transparent'
  },
  userCaret: {
    marginTop: '10px'
  },
  caretRTL: {
    left: '11px',
    right: 'auto'
  },
  caretActive: {
    transform: 'rotate(180deg)'
  },
  purple: {
    '&,&:hover,&:focus': {
      color: '#FFFFFF',
      backgroundColor: primaryColor,
      ...primaryBoxShadow
    }
  },
  blue: {
    '&,&:hover,&:focus': {
      color: '#FFFFFF',
      backgroundColor: infoColor,
      boxShadow:
        '0 12px 20px -10px rgba(0,188,212,.28), 0 4px 20px 0 rgba(0,0,0,.12), 0 7px 8px -5px rgba(0,188,212,.2)'
    }
  },
  green: {
    '&,&:hover,&:focus': {
      color: '#FFFFFF',
      backgroundColor: successColor,
      boxShadow:
        '0 12px 20px -10px rgba(76,175,80,.28), 0 4px 20px 0 rgba(0,0,0,.12), 0 7px 8px -5px rgba(76,175,80,.2)'
    }
  },
  orange: {
    '&,&:hover,&:focus': {
      color: '#FFFFFF',
      backgroundColor: warningColor,
      boxShadow:
        '0 12px 20px -10px rgba(255,152,0,.28), 0 4px 20px 0 rgba(0,0,0,.12), 0 7px 8px -5px rgba(255,152,0,.2)'
    }
  },
  red: {
    '&,&:hover,&:focus': {
      color: '#FFFFFF',
      backgroundColor: dangerColor,
      boxShadow:
        '0 12px 20px -10px rgba(244,67,54,.28), 0 4px 20px 0 rgba(0,0,0,.12), 0 7px 8px -5px rgba(244,67,54,.2)'
    }
  },
  liteseagreen: {
    '&,&:hover,&:focus': {
      color: cornsilkColor,
      backgroundColor: liteseagreenColor,
      boxShadow:
        '0 12px 20px -10px rgba(244,67,54,.28), 0 4px 20px 0 rgba(0,0,0,.12), 0 7px 8px -5px rgba(244,67,54,.2)'
    }
  },
  salmon: {
    '&,&:hover,&:focus': {
      color: '#FFFFFF',
      backgroundColor: salmonColor,
      boxShadow:
        '0 12px 20px -10px rgba(244,67,54,.28), 0 4px 20px 0 rgba(0,0,0,.12), 0 7px 8px -5px rgba(244,67,54,.2)'
    }
  },
  white: {
    '&,&:hover,&:focus': {
      color: '#3C4858',
      backgroundColor: '#FFFFFF',
      boxShadow:
        '0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(60,72,88,.4)'
    }
  },
  rose: {
    '&,&:hover,&:focus': {
      color: '#FFFFFF',
      backgroundColor: roseColor,
      boxShadow:
        '0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(233,30,99,.4)'
    }
  },

  sidebarWrapper: {
    position: 'relative',
    height: 'calc(100vh - 75px)',
    overflow: 'auto',
    width: '260px',
    zIndex: '4',
    overflowScrolling: 'touch',
    transitionProperty: 'top, bottom, width',
    transitionDuration: '.2s, .2s, .35s',
    transitionTimingFunction: 'linear, linear, ease',
    color: 'inherit',
    paddingBottom: '30px'
  },
  sidebarWrapperWithPerfectScrollbar: {
    overflow: 'hidden !important'
  },
  user: {
    paddingBottom: '20px',
    margin: '20px auto 0',
    position: 'relative',
    '&:after': {
      content: '""',
      position: 'absolute',
      bottom: '0',
      right: '15px',
      height: '1px',
      width: 'calc(100% - 30px)',
      backgroundColor: 'hsla(0,0%,100%,.3)'
    }
  },
  photo: {
    transition: 'all 300ms linear',
    width: '34px',
    height: '34px',
    overflow: 'hidden',
    float: 'left',
    zIndex: '5',
    marginRight: '11px',
    marginLeft: '23px',
    ...boxShadow
  },
  photoRTL: {
    float: 'right',
    marginLeft: '12px',
    marginRight: '24px'
  },
  avatar: {
    width: '100%',
    verticalAlign: 'middle',
    border: '0',
    color: 'white'
  },
  userCollapseButton: {
    margin: '0',
    padding: '6px 15px',
    '&:hover': {
      background: 'none'
    }
  },
  userCollapseLinks: {
    marginTop: '-4px',
    '&:hover,&:focus': {
      color: '#FFFFFF'
    }
  }
});

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openAvatar: false,
      openComponents: this.activeRoute('/components'),
      openForms: this.activeRoute('/forms'),
      openTables: this.activeRoute('/tables'),
      openMaps: this.activeRoute('/maps'),
      openPages: this.activeRoute('-page'),
      miniActive: true
    };
    this.activeRoute.bind(this);
  }

  activeRoute(routeName) {
    const { location } = this.props;
    return location.pathname.indexOf(routeName) > -1;
  }

  openCollapse(collapse) {
    const st = {};
    st[collapse] = !this.state[collapse];
    this.setState(st);
  }

  render() {
    const {
      classes,
      color,
      logo,
      image,
      logoText,
      routes,
      bgColor,
      rtlActive,
      miniActive,
      handleDrawerToggle,
      open
    } = this.props;
    const { miniActive: miniActiveState, openAvatar } = this.state;
    const itemText = `${classes.itemText} ${classNames({
      [classes.itemTextMini]: miniActive && miniActiveState,
      [classes.itemTextMiniRTL]: rtlActive && miniActive && miniActiveState,
      [classes.itemTextRTL]: rtlActive
    })}`;
    const collapseItemText = `${classes.collapseItemText} ${classNames({
      [classes.collapseItemTextMini]: miniActive && miniActiveState,
      [classes.collapseItemTextMiniRTL]:
        rtlActive && miniActive && miniActiveState,
      [classes.collapseItemTextRTL]: rtlActive
    })}`;
    const userWrapperClass = `${classes.user} ${classNames({
      [classes.whiteAfter]: bgColor === 'white'
    })}`;
    const caret = `${classes.caret} ${classNames({
      [classes.caretRTL]: rtlActive
    })}`;
    const collapseItemMini = `${classes.collapseItemMini} ${classNames({
      [classes.collapseItemMiniRTL]: rtlActive
    })}`;
    const photo = `${classes.photo} ${classNames({
      [classes.photoRTL]: rtlActive
    })}`;
    const user = undefined;
    const links = (
      <List className={classes.list}>
        {routes.map((prop, key) => {
          if (prop.redirect) {
            return null;
          }
          if (prop.collapse) {
            const navLinkClasses = `${classes.itemLink} ${classNames({
              [` ${classes.collapseActive}`]: this.activeRoute(prop.path)
            })}`;
            const itemText = `${classes.itemText} ${classNames({
              [classes.itemTextMini]: miniActive && miniActiveState,
              [classes.itemTextMiniRTL]:
                rtlActive && miniActive && miniActiveState,
              [classes.itemTextRTL]: rtlActive
            })}`;
            const collapseItemText = `${classes.collapseItemText} ${classNames({
              [classes.collapseItemTextMini]: miniActive && miniActiveState,
              [classes.collapseItemTextMiniRTL]:
                rtlActive && miniActive && miniActiveState,
              [classes.collapseItemTextRTL]: rtlActive
            })}`;
            const itemIcon = `${classes.itemIcon} ${classNames({
              [classes.itemIconRTL]: rtlActive
            })}`;
            const caret = `${classes.caret} ${classNames({
              [classes.caretRTL]: rtlActive
            })}`;
            return (
              <ListItem key={key} className={classes.item}>
                <NavLink
                  to="#"
                  className={navLinkClasses}
                  onClick={() => this.openCollapse(prop.state)}
                >
                  <ListItemIcon className={itemIcon}>
                    {typeof prop.icon === 'string' ? (
                      <Icon>{prop.icon}</Icon>
                    ) : (
                      <prop.icon />
                    )}
                  </ListItemIcon>
                  <ListItemText
                    primary={prop.name}
                    secondary={
                      <b
                        className={`${caret} ${
                          this.state[prop.state] ? classes.caretActive : ''
                        }`}
                      />
                    }
                    disableTypography
                    className={itemText}
                  />
                </NavLink>
                <Collapse in={this.state[prop.state]} unmountOnExit>
                  <List className={`${classes.list} ${classes.collapseList}`}>
                    {prop.views.map((prop, key) => {
                      if (prop.redirect) {
                        return null;
                      }
                      const navLinkClasses = `${
                        classes.collapseItemLink
                      } ${classNames({
                        [` ${classes[color]}`]: this.activeRoute(prop.path)
                      })}`;
                      const collapseItemMini = `${
                        classes.collapseItemMini
                      } ${classNames({
                        [classes.collapseItemMiniRTL]: rtlActive
                      })}`;
                      return (
                        <ListItem key={key} className={classes.collapseItem}>
                          <NavLink to={prop.path} className={navLinkClasses}>
                            <span className={collapseItemMini}>
                              {prop.mini}
                            </span>
                            <ListItemText
                              primary={prop.name}
                              disableTypography
                              className={collapseItemText}
                            />
                          </NavLink>
                        </ListItem>
                      );
                    })}
                  </List>
                </Collapse>
              </ListItem>
            );
          }
          const navLinkClasses = `${classes.itemLink} ${classNames({
            [` ${classes[color]}`]: this.activeRoute(prop.path)
          })}`;
          const itemText = `${classes.itemText} ${classNames({
            [classes.itemTextMini]: miniActive && miniActiveState,
            [classes.itemTextMiniRTL]:
              rtlActive && miniActive && miniActiveState,
            [classes.itemTextRTL]: rtlActive
          })}`;
          const itemIcon = `${classes.itemIcon} ${classNames({
            [classes.itemIconRTL]: rtlActive
          })}`;
          return (
            <ListItem key={key} className={classes.item}>
              <NavLink to={prop.path} className={navLinkClasses}>
                <ListItemIcon className={itemIcon}>
                  {typeof prop.icon === 'string' ? (
                    <Icon>{prop.icon}</Icon>
                  ) : (
                    <prop.icon />
                  )}
                </ListItemIcon>
                <ListItemText
                  primary={prop.name}
                  disableTypography
                  className={itemText}
                />
              </NavLink>
            </ListItem>
          );
        })}
      </List>
    );

    const logoNormal = `${classes.logoNormal} ${classNames({
      [classes.logoNormalSidebarMini]: miniActive && miniActiveState,
      [classes.logoNormalSidebarMiniRTL]:
        rtlActive && miniActive && miniActiveState,
      [classes.logoNormalRTL]: rtlActive
    })}`;
    const logoMini = `${classes.logoMini} ${classNames({
      [classes.logoMiniRTL]: rtlActive
    })}`;
    const logoClasses = `${classes.logo} ${classNames({
      [classes.whiteAfter]: bgColor === 'white'
    })}`;
    const brand = (
      <div className={logoClasses}>
        <a href="/" className={logoMini}>
          <img src={logo} alt="logo" className={classes.img} />
        </a>
        <a href="/" className={logoNormal}>
          {logoText}
        </a>
      </div>
    );
    const drawerPaper = `${classes.drawerPaper} ${classNames({
      [classes.drawerPaperMini]: miniActive && miniActiveState,
      [classes.drawerPaperRTL]: rtlActive
    })}`;
    const sidebarWrapper = `${classes.sidebarWrapper} ${classNames({
      [classes.drawerPaperMini]: miniActive && miniActiveState,
      [classes.sidebarWrapperWithPerfectScrollbar]:
        navigator.platform.indexOf('Win') > -1
    })}`;
    return (
      <div>
        <Hidden mdUp implementation="css">
          <Drawer
            variant="temporary"
            anchor={rtlActive ? 'left' : 'right'}
            open={open}
            classes={{
              paper: `${drawerPaper} ${classes[`${bgColor}Background`]}`
            }}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            {brand}
            <SidebarWrapper
              className={sidebarWrapper}
              user={user}
              headerLinks={<HeaderLinks rtlActive={rtlActive} />}
              links={links}
            />
            {image !== undefined ? (
              <div
                className={classes.background}
                style={{ backgroundImage: `url(${image})` }}
              />
            ) : null}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            onMouseOver={() => this.setState({ miniActive: false })}
            onMouseOut={() => this.setState({ miniActive: true })}
            anchor={rtlActive ? 'right' : 'left'}
            variant="permanent"
            open
            classes={{
              paper: `${drawerPaper} ${classes[`${bgColor}Background`]}`
            }}
          >
            {brand}
            <SidebarWrapper
              className={sidebarWrapper}
              user={user}
              links={links}
            />
            {image !== undefined ? (
              <div
                className={classes.background}
                style={{ backgroundImage: `url(${image})` }}
              />
            ) : null}
          </Drawer>
        </Hidden>
      </div>
    );
  }
}

export default withRouter(withStyles(sidebarStyle)(Sidebar));
