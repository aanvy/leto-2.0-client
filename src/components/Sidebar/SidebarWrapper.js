import React from 'react';

class SidebarWrapper extends React.Component {
  render() {
    const { className, user, headerLinks, links } = this.props;
    return (
      <div className={className}>
        {user}
        {headerLinks}
        {links}
      </div>
    );
  }
}

export default SidebarWrapper;
