import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import React from 'react';
import classNames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';

import {
  container,
  containerFluid,
  defaultFont,
  primaryColor
} from '../../assets/theme/leto';

const footerStyle = {
  block: {},
  left: {
    float: 'left!important',
    display: 'block'
  },
  right: {
    margin: '0',
    fontSize: '14px',
    float: 'right!important',
    padding: '15px'
  },
  footer: {
    bottom: '0',
    borderTop: '1px solid #e7e7e7',
    padding: '15px 0',
    ...defaultFont,
    zIndex: 4,
    float: 'right',
    width: '100%'
  },
  container: {
    zIndex: 3,
    ...container,
    position: 'relative'
  },
  containerFluid: {
    zIndex: 3,
    ...containerFluid,
    position: 'relative'
  },
  a: {
    color: primaryColor,
    textDecoration: 'none',
    backgroundColor: 'transparent'
  },
  list: {
    marginBottom: '0',
    padding: '0',
    marginTop: '0'
  },
  inlineBlock: {
    display: 'inline-block',
    padding: '5',
    width: 'auto'
  },
  whiteColor: {
    '&,&:hover,&:focus': {
      color: '#FFFFFF'
    }
  }
};
function Footer({ ...props }) {
  const { classes, fluid, white } = props;
  const footerContainer = classNames({
    [classes.container]: !fluid,
    [classes.containerFluid]: fluid,
    [classes.whiteColor]: white
  });
  const anchor =
    classes.a +
    classNames({
      [` ${classes.whiteColor}`]: white
    });
  const block = classNames({
    [classes.block]: true,
    [classes.whiteColor]: white
  });
  return (
    <footer className={classes.footer}>
      <div className={footerContainer}>
        <div className={classes.left}>
          <List className={classes.list}>
            <ListItem className={classes.inlineBlock}>
              <a
                href="https://betsol.com"
                target="_blank"
                rel="noopener noreferrer"
                className={block}
              >
                Company
              </a>
            </ListItem>
            <ListItem className={classes.inlineBlock}>
              <a
                href="https://betsol.com/betsol-blog/"
                target="_blank"
                rel="noopener noreferrer"
                className={block}
              >
                Blog
              </a>
            </ListItem>
          </List>
        </div>
        <p className={classes.right}>
          &copy; 2018{' '}
          <a href="https://betsol.com/leto-devops/" className={anchor}>
            Betsol LeTo
          </a>
        </p>
      </div>
    </footer>
  );
}

export default withStyles(footerStyle)(Footer);
