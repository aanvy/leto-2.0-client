import { withRouter } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Hidden from '@material-ui/core/Hidden';
import Menu from '@material-ui/icons/Menu';
import MoreVert from '@material-ui/icons/MoreVert';
import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import ViewList from '@material-ui/icons/ViewList';
import classNames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';

import {
  containerFluid,
  dangerColor,
  defaultBoxShadow,
  defaultFont,
  infoColor,
  primaryColor,
  successColor,
  warningColor
} from '../../assets/theme/leto';
import Button from '../custom-buttons/Button';
import HeaderLinks from './HeaderLinks';

const headerStyle = () => ({
  appBar: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
    borderBottom: '0',
    marginBottom: '0',
    position: 'absolute',
    width: '100%',
    paddingTop: '10px',
    zIndex: '1029',
    color: '#555555',
    border: '0',
    borderRadius: '3px',
    padding: '10px 0',
    transition: 'all 150ms ease 0s',
    minHeight: '50px',
    display: 'block'
  },
  container: {
    ...containerFluid,
    minHeight: '50px'
  },
  flex: {
    flex: 1
  },
  title: {
    ...defaultFont,
    lineHeight: '30px',
    fontSize: '18px',
    borderRadius: '3px',
    textTransform: 'none',
    color: 'inherit',
    paddingTop: '0.625rem',
    paddingBottom: '0.625rem',
    margin: '0 !important',
    '&:hover,&:focus': {
      background: 'transparent'
    }
  },
  primary: {
    backgroundColor: primaryColor,
    color: '#FFFFFF',
    ...defaultBoxShadow
  },
  info: {
    backgroundColor: infoColor,
    color: '#FFFFFF',
    ...defaultBoxShadow
  },
  success: {
    backgroundColor: successColor,
    color: '#FFFFFF',
    ...defaultBoxShadow
  },
  warning: {
    backgroundColor: warningColor,
    color: '#FFFFFF',
    ...defaultBoxShadow
  },
  danger: {
    backgroundColor: dangerColor,
    color: '#FFFFFF',
    ...defaultBoxShadow
  },
  sidebarMinimize: {
    float: 'left',
    padding: '0 0 0 15px',
    display: 'block',
    color: '#555555'
  },
  sidebarMinimizeRTL: {
    padding: '0 15px 0 0 !important'
  },
  sidebarMiniIcon: {
    width: '20px',
    height: '17px'
  }
});

function Header({ ...props }) {
  const { miniActive, sidebarMinimize, handleDrawerToggle } = props;
  function makeBrand() {
    let name;
    props.routes.map(prop => {
      if (prop.collapse) {
        prop.views.map(propChild => {
          if (propChild.path === props.location.pathname) {
            const { name: aliasName } = propChild;
            name = aliasName;
          }
          return null;
        });
      }
      if (prop.path === props.location.pathname) {
        const { name: aliasName } = prop;
        name = aliasName;
      }
      return null;
    });
    if (name) {
      return name;
    }
    return 'Default Brand Name';
  }
  const { classes, color, rtlActive } = props;
  const appBarClasses = classNames({
    [` ${classes[color]}`]: color
  });
  const sidebarMinimizeSecond = `${classes.sidebarMinimize} ${classNames({
    [classes.sidebarMinimizeRTL]: rtlActive
  })}`;
  return (
    <AppBar className={classes.appBar + appBarClasses}>
      <Toolbar className={classes.container}>
        <Hidden smDown implementation="css">
          <div className={sidebarMinimizeSecond}>
            {miniActive ? (
              <Button justIcon round color="white" onClick={sidebarMinimize}>
                <ViewList className={classes.sidebarMiniIcon} />
              </Button>
            ) : (
              <Button justIcon round color="white" onClick={sidebarMinimize}>
                <MoreVert className={classes.sidebarMiniIcon} />
              </Button>
            )}
          </div>
        </Hidden>
        <div className={classes.flex}>
          {/* Here we create navbar brand, based on route name */}
          <Button href="#" className={classes.title} color="transparent">
            {makeBrand()}
          </Button>
        </div>
        <Hidden smDown implementation="css">
          <HeaderLinks rtlActive={rtlActive} />
        </Hidden>
        <Hidden mdUp implementation="css">
          <Button
            className={classes.appResponsive}
            color="transparent"
            justIcon
            aria-label="open drawer"
            onClick={handleDrawerToggle}
          >
            <Menu />
          </Button>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}

export default withRouter(withStyles(headerStyle)(Header));
