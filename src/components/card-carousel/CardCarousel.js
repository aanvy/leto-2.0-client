import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import Paper from '@material-ui/core/Paper';
import React, { PureComponent } from 'react';
import Slider from 'react-slick';

import MediaCard from './MediaCard';

class CardCarousal extends PureComponent {
  renderData() {
    const { data, onItemClick } = this.props;
    if (this.props && data && data.length && data.length >= 3) {
      return data.map((carouselData, index) => (
        <MediaCard
          key={index}
          name={carouselData.name}
          workflowCount={carouselData.workflowCount}
          handleClick={() => onItemClick(carouselData)}
        />
      ));
    }
    if (this.props && data && data.length && data.length < 3) {
      const carouselData = [...data];
      const amtOfPlaceholders = 3 - data.length;

      // push placeholders to avoid card carousel from rendering strangely when there's less than 3 items
      for (let i = 0; i < amtOfPlaceholders; i += 1) {
        carouselData.push({ placeholder: true });
      }

      return carouselData.map((carouselDataItem, index) => {
        if (carouselDataItem.placeholder) {
          return <MediaCard key={index} placeholder />;
        }
        return (
          <MediaCard
            key={index}
            name={carouselDataItem.name}
            handleClick={() => onItemClick(carouselDataItem)}
          />
        );
      });
    }
    return (
      <div className="carousel-item">
        <h3>No data found</h3>
      </div>
    );
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 250,
      slidesToShow: 4,
      slidesToScroll: 1,
      swipeToSlide: true,
      responsive: [
        {
          breakpoint: 1335,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 905,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        }
      ]
    };

    return (
      <Paper
        elevation={1}
        classes={{ root: 'card-carousel-container ' }}
        style={{ boxShadow: 'none' }}
      >
        <Slider {...settings}>{this.renderData()}</Slider>
      </Paper>
    );
  }
}

export default CardCarousal;
