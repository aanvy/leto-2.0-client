import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import ProjectIcon from '@material-ui/icons/WorkOutline';
import React from 'react';

import { cardTitle } from '../../assets/theme/leto';
import CardFooter from '../card/CardFooter';
import CardHeader from '../card/CardHeader';
import CardIcon from '../card/CardIcon';

const styles = {
  root: {
    height: 20,
    maxWidth: 20
  },
  card: {
    maxWidth: 230,
    margin: '0 auto',
    height: 120,
    overflow: 'visible',
    boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.1)',
    '&:hover': {
      boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.8)'
    }
  },
  cardCategory: {
    color: '#999999',
    fontSize: '14px',
    paddingTop: '10px',
    marginBottom: '0',
    marginTop: '0',
    margin: '0'
  },
  cardTitle: {
    ...cardTitle,
    marginTop: '0px',
    marginBottom: '3px'
  }
};

function MediaCard(props) {
  const { classes, name, handleClick, placeholder, workflowCount } = props;
  return (
    <Card
      className={classes.card}
      classes={{ root: placeholder ? 'visibility-hidden' : '' }}
    >
      <div style={{ cursor: 'pointer' }} onClick={handleClick}>
        <CardHeader color="primary" stats icon>
          <CardIcon
            color="primary"
            style={{ backgroundColor: 'rgb(52, 69, 99)' }}
          >
            <ProjectIcon />
          </CardIcon>
          <p className={classes.cardCategory}>Workflows</p>
          <h3 className={classes.cardTitle}>{workflowCount}</h3>
        </CardHeader>
        <CardFooter stats>
          <div className={classes.stats}>{name}</div>
        </CardFooter>
      </div>
    </Card>
  );
}
export default withStyles(styles)(MediaCard);
