import DashboardIcon from '@material-ui/icons/Dashboard';
import HelpIcon from '@material-ui/icons/HelpOutline';
import ProjectIcon from '@material-ui/icons/WorkOutline';
import ReportingIcon from '@material-ui/icons/BarChart';

import DashboardPage from '../views/DashboardPage';
import FAQPage from '../views/FAQPage';
import ProjectsPage from '../views/ProjectsPage';
import ReportsPage from '../views/ReportsPage';

const dashRoutes = [
  {
    path: '/dashboard',
    name: 'Dashboard',
    icon: DashboardIcon,
    component: DashboardPage
  },
  {
    path: '/projects',
    name: 'Projects',
    icon: ProjectIcon,
    component: ProjectsPage
  },
  {
    path: '/reports',
    name: 'Reports',
    icon: ReportingIcon,
    component: ReportsPage
  },
  {
    path: '/faq',
    name: 'FAQ',
    icon: HelpIcon,
    component: FAQPage
  },
  { redirect: true, path: '/', pathTo: '/dashboard', name: 'Dashboard' }
];
export default dashRoutes;
