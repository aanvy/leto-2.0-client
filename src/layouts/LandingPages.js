import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

import Footer from '../components/footer/Footer';
import PagesHeader from '../components/header/PagesHeader';
import bgImage from '../assets/img/register.jpeg';
import pagesRoutes from '../routes/landing-pages';

const pagesStyle = theme => ({
  wrapper: {
    height: 'auto',
    minHeight: '100vh',
    position: 'relative',
    top: '0'
  },
  fullPage: {
    position: 'relative',
    minHeight: '100vh',
    display: 'flex!important',
    margin: '0',
    border: '0',
    color: '#fff',
    alignItems: 'center',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    height: '100%',
    [theme.breakpoints.down('sm')]: {
      minHeight: 'fit-content!important'
    },
    '& footer': {
      position: 'absolute',
      bottom: '0',
      width: '100%',
      border: 'none !important'
    },
    '&:before': {
      backgroundColor: 'rgba(0, 0, 0, 0.65)'
    },
    '&:before,&:after': {
      display: 'block',
      content: '""',
      position: 'absolute',
      width: '100%',
      height: '100%',
      top: '0',
      left: '0',
      zIndex: '2'
    }
  }
});

class Pages extends React.Component {
  componentDidMount() {
    document.body.style.overflow = 'unset';
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <PagesHeader {...rest} />
        <div className={classes.wrapper}>
          <div
            className={classes.fullPage}
            style={{ backgroundImage: `url(${bgImage})` }}
          >
            <Switch>
              {pagesRoutes.map((prop, key) => {
                if (prop.collapse) {
                  return null;
                }
                if (prop.redirect) {
                  return (
                    <Redirect from={prop.path} to={prop.pathTo} key={key} />
                  );
                }
                return (
                  <Route
                    path={prop.path}
                    component={prop.component}
                    key={key}
                  />
                );
              })}
              <Route exact path="*" render={() => <Redirect to="/login" />} />
            </Switch>
            <Footer white />
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(withStyles(pagesStyle)(Pages));
