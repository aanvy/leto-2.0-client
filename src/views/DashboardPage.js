import CircularProgress from '@material-ui/core/CircularProgress';
import InputAdornment from '@material-ui/core/InputAdornment';
import PlayIcon from '@material-ui/icons/PlayCircleOutline';
import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import WarningIcon from '@material-ui/icons/Warning';
import withStyles from '@material-ui/core/styles/withStyles';

import { cardTitle, successColor, tooltip } from '../assets/theme/leto';
import Card from '../components/card/Card';
import CardBody from '../components/card/CardBody';
import CardCarousel from '../components/card-carousel/CardCarousel';
import CardHeader from '../components/card/CardHeader';
import GridContainer from '../components/grid/GridContainer';
import GridItem from '../components/grid/GridItem';
import Table from '../components/table/Table';
import successImage from '../assets/img/success.png';

const projects = [
  {
    name: 'Project1',
    workflowCount: 6
  },
  {
    name: 'Project2',
    workflowCount: 5
  },
  {
    name: 'Project3',
    workflowCount: 18
  },
  {
    name: 'Project4',
    workflowCount: 10
  },
  {
    name: 'Project5',
    workflowCount: 20
  }
];
const hoverCardStyle = {
  cardHover: {
    '&:hover': {
      '& $cardHeaderHover': {
        transform: 'translate3d(0, -50px, 0)'
      }
    }
  },
  cardHeaderHover: {
    transition: 'all 300ms cubic-bezier(0.34, 1.61, 0.7, 1)'
  },
  cardHoverUnder: {
    position: 'absolute',
    zIndex: '1',
    top: '-50px',
    width: 'calc(100% - 30px)',
    left: '17px',
    right: '17px',
    textAlign: 'center'
  }
};
const dashboardStyle = {
  ...hoverCardStyle,
  tooltip,
  cardTitle: {
    ...cardTitle,
    marginTop: '0px',
    marginBottom: '3px'
  },
  cardShadow: {
    boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.14)'
  },
  cardIconTitle: {
    ...cardTitle,
    marginTop: '15px',
    marginBottom: '0px'
  },
  cardProductTitle: {
    ...cardTitle,
    marginTop: '0px',
    marginBottom: '3px',
    textAlign: 'center'
  },
  cardCategory: {
    color: '#999999',
    fontSize: '14px',
    paddingTop: '10px',
    marginBottom: '0',
    marginTop: '0',
    margin: '0'
  },
  cardProductDesciprion: {
    textAlign: 'center',
    color: '#999999'
  },
  stats: {
    color: '#999999',
    fontSize: '12px',
    lineHeight: '22px',
    display: 'inline-flex',
    '& svg': {
      position: 'relative',
      top: '4px',
      width: '16px',
      height: '16px',
      marginRight: '3px'
    },
    '& .fab,& .fas,& .far,& .fal,& .material-icons': {
      position: 'relative',
      top: '4px',
      fontSize: '16px',
      marginRight: '3px'
    }
  },
  productStats: {
    paddingTop: '7px',
    paddingBottom: '7px',
    margin: '0'
  },
  successText: {
    color: successColor
  },
  upArrowCardCategory: {
    width: 14,
    height: 14
  },
  underChartIcons: {
    width: '17px',
    height: '17px'
  },
  price: {
    color: 'inherit',
    '& h4': {
      marginBottom: '0px',
      marginTop: '0px'
    }
  },
  new: {
    padding: '15px 0 15px 30px'
  },
  actionButton: {
    margin: '0 0 0 5px',
    padding: '5px',
    '& svg,& .fab,& .fas,& .far,& .fal,& .material-icons': {
      marginRight: '0px'
    }
  },
  icon: {
    verticalAlign: 'middle',
    width: '17px',
    height: '17px',
    top: '-1px',
    position: 'relative'
  },
  right: {
    textAlign: 'right'
  },
  center: {
    textAlign: 'center'
  },
  dashboardTitle: {
    padding: 10
  },
  play: {
    height: 16,
    width: 16,
    color: 'rgb(52, 69, 99)'
  }
};

class DashboardPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      rowsPerPage: 5
    };
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
  }

  onFocusExe() {
    const state = { ...this.state };
    state.widthExe = '100%';
    this.setState(state);
  }

  onBlurExe() {
    const state = { ...this.state };
    state.widthExe = '70%';
    this.setState(state);
  }

  onFocusWork() {
    const state = { ...this.state };
    state.widthWork = '100%';
    this.setState(state);
  }

  onBlurWork() {
    const state = { ...this.state };
    state.widthWork = '70%';
    this.setState(state);
  }

  handleChangePage(event, page) {
    this.setState({ page });
  }

  handleChangeRowsPerPage(event) {
    this.setState({ rowsPerPage: event.target.value });
  }

  render() {
    const { rowsPerPage, page, widthExe, widthWork } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.new}>
        <GridContainer>
          <GridItem item xs={12} sm={12}>
            <div style={{ marginRight: '30px' }}>
              <Typography
                variant="subheading"
                gutterBottom
                style={{ marginLeft: '20px', marginBottom: '20px' }}
              >
                Recent Projects
              </Typography>
              <CardCarousel data={projects} />
            </div>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={12} md={7}>
            <Card>
              <CardHeader color="rose" icon className={classes.dashboardTitle}>
                <GridContainer>
                  <GridItem xs={6} sm={4} md={4}>
                    <Typography
                      variant="subheading"
                      gutterBottom
                      style={{ padding: '25px 25px 25px 0' }}
                    >
                      Recent Executions
                    </Typography>
                  </GridItem>
                  <GridItem xs={6} sm={8} md={8} style={{ textAlign: 'right' }}>
                    <TextField
                      style={{ width: widthExe, height: '40px' }}
                      className="transInput"
                      id="searchItems"
                      label="Search"
                      margin="dense"
                      onFocus={() => this.onFocusExe()}
                      onBlur={() => this.onBlurExe()}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment>
                            <SearchIcon style={{ margin: '0 0 5px 0' }} />
                          </InputAdornment>
                        )
                      }}
                    />
                  </GridItem>
                </GridContainer>
              </CardHeader>
              <CardBody>
                <Table
                  rowsPerPage={rowsPerPage}
                  page={page}
                  handleChangePage={this.handleChangePage}
                  tableHead={['', 'Workflow', 'Project', 'Start Time']}
                  tableData={[
                    [
                      <CircularProgress
                        className={classes.progress}
                        size={16}
                        thickness={5}
                      />,
                      'Workflow1',
                      'Project1',
                      '12/12/2018'
                    ],
                    [
                      <CircularProgress
                        className={classes.progress}
                        size={16}
                        thickness={5}
                      />,
                      'Workflow2',
                      'Project2',
                      '18/12/2018'
                    ],
                    [
                      <img src={successImage} alt="." />,
                      'Workflow3',
                      'Project3',
                      '01/01/2019'
                    ],
                    [
                      <WarningIcon className={classes.dangerIcon} />,
                      'Workflow4',
                      'Project4',
                      '02/01/2019'
                    ],
                    [
                      <img src={successImage} alt="." />,
                      'Workflow5',
                      'Project5',
                      '04/01/2019'
                    ]
                  ]}
                />
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={5}>
            <Card>
              <CardHeader color="rose" icon className={classes.dashboardTitle}>
                <GridContainer>
                  <GridItem xs={6} sm={5} md={5}>
                    <Typography
                      variant="subheading"
                      gutterBottom
                      style={{ padding: '25px 25px 25px 0' }}
                    >
                      Workflows
                    </Typography>
                  </GridItem>
                  <GridItem xs={6} sm={7} md={7} style={{ textAlign: 'right' }}>
                    <TextField
                      style={{ width: widthWork, height: '40px' }}
                      id="searchItems"
                      className="transInput"
                      label="Search"
                      margin="dense"
                      onFocus={() => this.onFocusWork()}
                      onBlur={() => this.onBlurWork()}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment>
                            <SearchIcon style={{ margin: '0 0 5px 0' }} />
                          </InputAdornment>
                        )
                      }}
                    />
                  </GridItem>
                </GridContainer>
              </CardHeader>
              <CardBody>
                <Table
                  rowsPerPage={rowsPerPage}
                  page={page}
                  handleChangePage={this.handleChangePage}
                  tableHead={['Name', 'Project', '']}
                  tableData={[
                    [
                      'workflow1',
                      'Project1',
                      <PlayIcon className={classes.play} />
                    ],
                    [
                      'workflow2',
                      'Project1',
                      <PlayIcon className={classes.play} />
                    ],
                    [
                      'workflow3',
                      'Project2',
                      <PlayIcon className={classes.play} />
                    ],
                    [
                      'workflow4',
                      'Project2',
                      <PlayIcon className={classes.play} />
                    ],
                    [
                      'workflow5',
                      'Project5',
                      <PlayIcon className={classes.play} />
                    ]
                  ]}
                  customCellClasses={[
                    classes.center,
                    classes.right,
                    classes.right
                  ]}
                  customClassesForCells={[0, 4, 5]}
                  customHeadCellClasses={[
                    classes.center,
                    classes.right,
                    classes.right
                  ]}
                  customHeadClassesForCells={[0, 4, 5]}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(dashboardStyle)(DashboardPage);
