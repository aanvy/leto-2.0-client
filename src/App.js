import { BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import React, { Component, Fragment } from 'react';

import { getUsers } from './actions/Actions';
import Dashboard from './layouts/Views';
import Pages from './layouts/LandingPages';

class App extends Component {
  componentWillMount() {
    const { props } = this;
    props.dispatch(getUsers());
  }

  render() {
    const { isAuthenticated } = this.props;
    return (
      <Fragment>
        {!isAuthenticated && (
          <BrowserRouter>
            <Pages />
          </BrowserRouter>
        )}
        {isAuthenticated && (
          <BrowserRouter>
            <Dashboard />
          </BrowserRouter>
        )}
      </Fragment>
    );
  }
}

const select = state => ({
  users: state.auth.users,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(select)(App);
